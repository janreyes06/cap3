const {
	GraphQLObjectType,
	GraphQLID,
	GraphQLString,
	GraphQLList,
	GraphQLSchema,
	GraphQLNonNull

} = require('graphql')

const graphqlISODate = require('graphql-iso-date')
const {
	GraphQLDateTime
} = graphqlISODate

const User = require('../models/user')
const Transaction = require('../models/transaction')

/*GQL Object Type*/
const UserType = new GraphQLObjectType({
	name: 'User',
	fields: ()=> ({
		id: {type: GraphQLID},
		name: {type: GraphQLString},
		email: {type: GraphQLString},
		userName: {type: GraphQLString},
		password: {type: GraphQLString},
		createdAt: {type : GraphQLDateTime},
		updatedAt: {type: GraphQLDateTime}
	}) 
})

const TransactionType = new GraphQLObjectType({
	name: 'Transaction',
	fields: ()=> ({
		id: {type: GraphQLID},
		eventDate: {type: GraphQLString},
		createdAt: {type : GraphQLDateTime},
		updatedAt: {type: GraphQLDateTime}
	})
})





/*GQL Root Query*/

const RootQuery = new GraphQLObjectType({
	name: 'Query',
	fields: {
		users: {
			type: new GraphQLList(UserType),
			resolve: (parent, args)=> {
				return User.find({})
			}
		},
		user: {
			type: UserType,
			args: {
				id: {type: GraphQLID}
			},
			resolve: (parent, args)=> {
				return User.findById(args.id)
			}
		},
		transactions: {
			type: new GraphQLList(TransactionType),
			resolve: (parent, args)=> {
				return Transaction.find({})
			}
		},
		transaction: {
			type: TransactionType,
			args: {
				id: {type: GraphQLID}
			},
			resolve: (parent, args)=> {
				return Transaction.findById(args.id)
			}
		}
	}
})



/*Mutations*/


const Mutation = new GraphQLObjectType({
	name: 'Mutation',
	fields: {

		addUser: {
			type: UserType,
			args: {
				name: {type: new GraphQLNonNull(GraphQLString)},
				email: {type: new GraphQLNonNull(GraphQLString)},
				userName: {type: new GraphQLNonNull(GraphQLString)},
				password: {type: new GraphQLNonNull(GraphQLString)}
			},
			resolve: (parent, args)=> {
				let newUser = new User({
					name: args.name,
					email: args.email,
					userName: args.userName,
					password: args.password,

				})
				return newUser.save()
			}
		},
		updateUser: {
			type: UserType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLID)},
				name: {type: new GraphQLNonNull(GraphQLString)},
				email: {type: new GraphQLNonNull(GraphQLString)},
				userName: {type: new GraphQLNonNull(GraphQLString)},
				password: {type: new GraphQLNonNull(GraphQLString)}
			},
			resolve: (parent, args)=> {
				let userId = {_id: args.id}
				let updates = {
					name: args.name,
					email: args.email,
					userName: args.userName,
					password: args.password
				}
				return User.findOneAndUpdate(userId, updates)
			}
		},

		deleteUser: {
			type: UserType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLID)}
			},
			resolve: (parent, args)=>{
				let userId = {_id: args.id}
				return User.findOneAndDelete(userId)
			}
		},

		addTransaction: {
			type: TransactionType,
			args: {
				eventDate: {type: new GraphQLNonNull(GraphQLString)}
			},
			resolve: (parent, args)=>{
				let newTransaction = new Transaction({
					eventDate: args.eventDate
				})
				return newTransaction.save()
			}
		},

		updateTransaction: {
			type: TransactionType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLID)},
				eventDate: {type: new GraphQLNonNull(GraphQLString)}
			},
			resolve: (parent, args)=>{
				let transactionId = {_id: args.id}
				let updates = {eventDate: args.eventDate}
				return Transaction.findOneAndUpdate(transactionId, updates)
			}
		},

		deleteTransaction: {
			type: TransactionType,
			args: {
				id: {type: new GraphQLNonNull(GraphQLID)}
			},
			resolve: (parent, args)=>{
				let transactionId = {_id: args.id}
				return Transaction.findOneAndDelete(transactionId)
			}
		}

	}
})













module.exports = new GraphQLSchema({
	query: RootQuery,
	mutation: Mutation
})