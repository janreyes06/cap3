const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')


/*for graphql playground*/
const graphqlHTTP = require('express-graphql')
const graphqlSchema =  require('./gql-schema/gql-types-queries-mutations')









/*Database connection*/
mongoose.connect('mongodb://localhost:27017/cap3', {
	useNewUrlParser:true,
	useUnifiedTopology: true,
	useFindAndModify: false
})
mongoose.connection.once('open', () => {
	console.log("Now connected to Local MongoDB Server")
})

/*Middlewares*/
app.use(cors())

/*gql playground*/
app.use('/graphql', graphqlHTTP({schema: graphqlSchema, graphiql:true}))


/*Server initialization*/
app.listen(4000, ()=>{
	console.log('Now serving on port 4000')
})
