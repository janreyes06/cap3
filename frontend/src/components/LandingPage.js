import React from 'react'
import ReactDOM from 'react-dom'
import styles from './mystyle.module.css'


const LandingPage = () => {
	return(
		<React.Fragment>
		
			<div className="container-fluid">
				<div className="row" id={styles.landing}>
					<div className="col-md-8 mx-auto text-center">
						<h1 className={styles.landingheading}>JPreyes Photography</h1>
						<p className={styles.landingquote}>“when people ask me what equipment I use – I tell them my eyes.”</p>
						<p>
							<a className="btn btn-light border border-dark rounded-pill">Book Now</a>
						</p>
					</div>
				</div>
			</div>


			<div className="container-fluid">
				<h2 className={styles.aboutheading}>About me</h2>
				<div className="row" id={styles.landingabout}>
					<div className="col-md-4 offset-md-1">
						<h2 className={styles.aboutgreeting}>Hello! I'm JP Reyes</h2>
						<p className={styles.aboutpara}>My style has been described as creative, fun, engaging, and relaxed. I specialize
						in weddings and event photography.</p>
						<p className={styles.aboutpara}>I look forward to meeting you and discussing your photography needs.</p>
					</div>
				</div>
			</div>


			<div className="container">
				<div className="row" id={styles.storyone}>
					<div className="col-md-4 offset-md-4">
						<p className={styles.storyonepara}>"working with Jpreyes is one of the best decision that my husband and I have made. He made our wedding day memory looks so alive and fabulous!"</p>
					</div>
				</div>
			</div>


			<div className="container">
				<div className="row" id={styles.storytwo}>
					<div className="col-md-4">

					</div>
				</div>
			</div>


			<div className="container">
				<div className="row" id={styles.storythree}>
					<div className="col-md-4">

					</div>
				</div>
			</div>

		</React.Fragment>
	)
}

export default LandingPage