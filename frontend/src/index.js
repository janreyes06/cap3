import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import LandingPage from './components/LandingPage'

const pageComponent = (

	<React.Fragment>
	
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={LandingPage}/>
			</Switch>
		</BrowserRouter>

	</React.Fragment>

)



ReactDOM.render( pageComponent, document.getElementById('root'));


